import requests
from bs4 import BeautifulSoup

class GoodreadsAPIClient():
    api_token = 'J3I4XCJwrj9NWMXSkJQ'

    def get_book_details(self, url):
        ss = ''
        if not url:
            raise Exception("InvalidGoodreadsURL")
        url = url +'?key=' + GoodreadsAPIClient.api_token

        try:
            response = requests.get(url)
        except Exception as e:
            raise Exception("InvalidGoodreadsURL")

        if response.status_code != 200:
            return {}

        details = {
            "title": '',
            "average_rating": '',
            "ratings_count": '',
            "num_pages": '',
            "image_url": '',
            "publication_year": '',
            "authors": ''
        }

        soup = BeautifulSoup(response.content, 'html.parser')
        if soup.find('book'):
            title = soup.find('book').find('title').text
            average_rating = soup.find('book').find('average_rating').text
            ratings_count = soup.find('book').find('ratings_count').text
            num_pages = soup.find('book').find('num_pages').text
            image_url = soup.find('book').find('image_url').text
            publication_year = soup.find('book').find('publication_year').text

            if soup.find('book').find('authors').find('author'):
                authors_count = soup.find('book').find('authors').find_all('author')
            for i in authors_count:
                ss = ss + str(i.find('name').text)+','

            authors=ss[:len(ss)-1]

        details = {
            "title": title,
            "average_rating": average_rating,
            "ratings_count": ratings_count,
            "num_pages": num_pages,
            "image_url": image_url,
            "publication_year": publication_year,
            "authors": authors
        }

        return details


if __name__ == "__main__":
    url = "https://www.goodreads.com/book/show/12067.Good_Omens"
    # url = "https://www.goodreads.com/book/show/12177850-a-song-of-ice-and-fire"
    url = "https://www.gooreads.com/book/show/22034.The_Godfather"
    # url = "https://www.gooreads.com/book/show/The_Godfather"

    obj = GoodreadsAPIClient()
    data = obj.get_book_details(url)
    print(data)